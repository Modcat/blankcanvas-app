# Blankcanvas front and back installation

In order to run Blankcanvas correctly you will need to install both the frontend and the backend repo's.

1. run the following commands to download the repo's

git clone git@gitlab.com:Modcat/blankcanvas-app-frontend.git
git clone git@gitlab.com:Modcat/blankcanvas-app-backend.git

2. Go into each directory and then run yarn (please always use yarn as the default for package management)
cd yarn (in each directory)

Your all set! Go to the root directory and run electron yarn start and everything will be good to go!

The UI folder is an old Vue project that has some useful code. IT WILL BE REMOVED!

#### More info

We use electron forge to create the builds, simply boot the app up on the choosen desktop environment and then type yarn make and hey presto you'll see an out folder with all the generated files!

tmp directory is used for the app itself and for storing temporary data in the repo as well. Persistant data should be using the built in chrome database.