(function() {
    const statusText = document.getElementById('status')

    const appBootCheck = {
        // autoUpdate: false, // To be implemented on bootup
        frontend: false,
        backend: false,
    }

    const ipcMessages = []

    const updateStatusText = () => {
        const appBootCheckStatus = {
            // autoUpdate: false, // To be implemented on bootup
            frontend: 'Frontend loading...',
            backend: 'Backend loading...',
        }
    }
    
    const checkBootStatus = () => {
        const frontendCheck = ipcMessages.find(message => message.match(/http:\/\/[0-9]/gi))
        const backendCheck = ipcMessages.find(message => message.match(/Feathers application started on http:\/\/localhost:3030/gi))

        if (frontendCheck) {
            appBootCheck.frontend = frontendCheck.split(/http:\/\//gi)[1].split(':')[1].match(/^[0-9]*/gi)[0]
        }

        if (backendCheck) {
            appBootCheck.backend = '3030'
        }
    }

    mainThread.recieve('logs', (event, arg) => {
        console.log(arg)
        ipcMessages.push(arg)
        checkBootStatus()
    })
})()
