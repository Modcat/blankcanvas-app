const path = require('path');
const { spawn } = require('child_process')
const cwd = path.join(__dirname, '').split('src')[0]
const kill = require('kill-port')

module.exports = async(win) => {

    await kill(50456, 'tcp')

    const frontend = spawn('yarn frontend', { shell: true, cwd });

    frontend.stdout.on('data', (data) => {
        win.webContents.send('logs', `@@frontend:stdout@@ ${data}`)
    });

    frontend.stderr.on('data', (data) => {
        win.webContents.send('logs', `@@frontend:stderr@@ ${data}`)
    });
}